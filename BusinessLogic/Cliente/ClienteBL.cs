﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using DTO.DTOs;
using DTO.Filtros;


namespace BusinessLogic.Cliente
{
    public class ClienteBL
    {

        ITPruebaTecnicaEntities Context;


        #region Contructor

        public ClienteBL()
        {
            Context = new ITPruebaTecnicaEntities();
        }
        
        #endregion Contructor

        #region Busqueda

        /// <summary>
        /// Buscar clientes en base a filtro de busqueda
        /// </summary>
        /// <param name="filtro">objeto que contiene los filtros de busqueda</param>
        /// <returns></returns>
        public List<ClienteDTO> BuscarClientes(FiltroCliente filtro = null)
        {
            try
            {

                var clientes = BuscarEntidadPorFiltros(filtro);

                var clientesDTO = clientes.Select(ConvertirADTO).ToList();
                
                return clientesDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }
      

        /// <summary>
        /// Método que encuentra clientes en base a filtros de busqueda
        /// </summary>
        /// <param name="filtro">objeto que contiene los filtros</param>
        /// <returns></returns>
        private IQueryable<DataAccess.Cliente> BuscarEntidadPorFiltros(FiltroCliente filtro)
        {
            try
            {
                IQueryable<DataAccess.Cliente> entidad =  Context.Cliente;

                if (filtro != null)
                {
                    if (filtro.IdCliente > 0)
                        entidad = entidad.Where(e => e.IdCliente == filtro.IdCliente);

                    if (!string.IsNullOrEmpty(filtro.ID))
                        entidad = entidad.Where(e => e.ID == filtro.ID);

                    if (!string.IsNullOrEmpty(filtro.Nombre))
                        entidad = entidad.Where(e => e.Nombre.Contains(filtro.Nombre));

                    if (!string.IsNullOrEmpty(filtro.Apellido))
                        entidad = entidad.Where(e => e.Apellido.Contains(filtro.Apellido));

                    if (filtro.Edad > 0)
                        entidad = entidad.Where(e => e.Edad >= filtro.Edad);

                    if (!string.IsNullOrEmpty(filtro.Correo))
                        entidad = entidad.Where(e => e.Correo.Contains(filtro.Correo));
                    
                }
                               

                return entidad;

            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion Busqueda

        #region CRUD
        /// <summary>
        /// Inserta datos del cliente
        /// </summary>
        /// <param name="dto">objeto con información del cliente</param>
        public bool InsertarCliente(ClienteDTO dto, ref string mensaje)
        {
            try
            {

                var clientes = BuscarEntidadPorFiltros(new FiltroCliente
                {
                    ID = dto.ID
                });

                //verificar que ID no este registrado
                if (clientes.Count() == 0)
                {

                    DataAccess.Cliente entidad = new DataAccess.Cliente();

                    entidad.ID = dto.ID;
                    entidad.Nombre = dto.Nombre;
                    entidad.Apellido = dto.Apellido;
                    entidad.FechaNacimiento = Convert.ToDateTime( dto.FechaNacimiento);
                    entidad.Edad = dto.Edad;
                    entidad.Correo = dto.Correo;
                    Context.Cliente.Add(entidad);

                    Context.SaveChanges();
                    return true;
                }

                mensaje = string.Format("Ya existe un cliente con ID: {0}", dto.ID);
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Actualiza datos del cliente
        /// </summary>
        /// <param name="dto">objeto con información del cliente a actualizar</param>
        /// <returns></returns>
        public bool EditarCliente(ClienteDTO dto, ref string mensaje)
        {
            try
            {

                if (string.IsNullOrEmpty( dto.OriginalID))
                {
                    mensaje = "Datos incompletos para realizar la edición";
                    return false;
                }

                var clientes = BuscarEntidadPorFiltros(new FiltroCliente {
                    ID = dto.OriginalID
                });

                //Si ha cambiado el ID verficar si este ya esta resgistrado en base de datos
                if (dto.ID != dto.OriginalID)
                {
                    //Si existe
                    if (BuscarEntidadPorFiltros(new FiltroCliente { ID = dto.ID }).Count() > 0)
                    {
                        mensaje = string.Format("Ya existe un cliente con ID: {0}", dto.ID);
                        return false;
                    }
                }

                if (clientes.Count() == 1)
                {
                    DataAccess.Cliente entidad  = clientes.First();
                    entidad.ID = dto.ID;
                    entidad.Nombre = dto.Nombre;
                    entidad.Apellido = dto.Apellido;
                    entidad.FechaNacimiento = Convert.ToDateTime(dto.FechaNacimiento);
                    entidad.Edad = dto.Edad;
                    entidad.Correo = dto.Correo;

                    Context.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Borra datos del cliente
        /// </summary>
        /// <param name="dto">objeto con información del cliente a borrar</param>
        /// <returns></returns>
        public bool BorrarCliente(ClienteDTO dto)
        {
            try
            {
                var clientes = BuscarEntidadPorFiltros(new FiltroCliente
                {
                    IdCliente = dto.IdCliente
                });

                if (clientes.Count() == 1)
                {
                    Context.Cliente.Remove(clientes.First());
                    Context.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion CRUD

        #region Auxiliares
       
        /// <summary>
        /// Mapea la entidad a DTO
        /// </summary>
        /// <param name="entidad">objeto a convertir</param>
        /// <returns></returns>
        public ClienteDTO ConvertirADTO(DataAccess.Cliente entidad)
        {
            try
            {
                ClienteDTO dto = null;

                if (entidad != null)
                    //Mapea la entidad cliente a DTO
                    dto = new ClienteDTO
                    {
                        IdCliente = entidad.IdCliente,
                        ID = entidad.ID,
                        Nombre = entidad.Nombre,
                        Apellido = entidad.Apellido,
                        Edad = entidad.Edad,
                        FechaNacimiento = entidad.FechaNacimiento.ToString(),
                        Correo = entidad.Correo
                    };

                return dto;

            }
            catch (Exception)
            {
                throw;
            }
        }
       
        #endregion

    }
}
