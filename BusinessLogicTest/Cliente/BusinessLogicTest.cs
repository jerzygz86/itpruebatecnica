﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.Filtros;
using DTO.DTOs;

namespace BusinessLogic.Cliente.Tests
{
    [TestClass()]
    public class BusinessLogicTest
    {
        [TestMethod()]
        public void BuscarClientesTest()
        {
            var bl = new ClienteBL();

            var clientes = bl.BuscarClientes(new FiltroCliente {
                Nombre = "Jerzy"

            });

            var cantidadEsperada = 1;

            Assert.AreEqual(cantidadEsperada, clientes.Count);
        }


    

        [TestMethod()]
        public void InsertarClienteTest()
        {
            var bl = new ClienteBL();
            string mensaje = string.Empty;

            var resultado = bl.InsertarCliente(new ClienteDTO
            {
                ID = "id4",
                Nombre = "Andrea",
                Apellido = "Quintero",
                Edad = 27,
                Correo = "andrea@email.com"
            }, ref mensaje);

            var resultadoEsperado = true;


            Assert.AreEqual(resultadoEsperado, resultado);
        }

        [TestMethod()]
        public void EditarClienteTest()
        {
            var bl = new ClienteBL();
            string mensaje = string.Empty;

            var resultado = bl.EditarCliente(new ClienteDTO
            {
                ID = "id7",
                Nombre = "Cesar",
                Apellido = "Millán",
                Edad = 42,
                OriginalID = "id2"
            }, ref mensaje);

            var resultadoEsperado = true;


            Assert.AreEqual(resultadoEsperado, resultado);
        }

        [TestMethod()]
        public void EliminarClienteTest()
        {
            var bl = new ClienteBL();

            var resultado = bl.BorrarCliente(new ClienteDTO
            {
                IdCliente = 5
            });

            var resultadoEsperado = true;


            Assert.AreEqual(resultadoEsperado, resultado);
        }

   
    }
}