﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Filtros
{
    /// <summary>
    /// Contiene los filtros por los cuales se puede buscar a un cliente
    /// </summary>
    [Serializable]
    public class FiltroCliente
    {
        public int IdCliente { get; set; }
        public string ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
        public string Correo { get; set; }
    }
}
