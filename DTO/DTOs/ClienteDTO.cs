﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTOs
{
    /// <summary>
    /// clase para transferencia de datos del cliente
    /// </summary>
    [Serializable]
    public class ClienteDTO
    {
        public int IdCliente { get; set; }
        public string ID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string FechaNacimiento { get; set; }
        public int Edad { get; set; }
        public string Correo { get; set; }

        /// <summary>
        /// Almacena ID Original 
        /// </summary>
        public string OriginalID { get; set; }
    }


}
