# README #

Prueba	de suficiencia	técnica (Impacto Tecnológico)

La solucion esta estructurado en cuatro capas Aplicacion Web, Logica de Negocios, Acceso a Datos y DTO(contiene las entidades para la tranferencia de información a traves de las capas)



### Base de datos ###
* Crear carpeta "ITPruebaTecnica" en unidad C: (es la ruta donde se creará la base de datos)
* Ejecutar script "01_Basedatos_DataInicial.sql" este se encuentra en proyecto "Database", carpeta "Scripts" (Este creará la base de datos y carga inicial de datos)


### Aplicación Web ###
* En el archivo web.config cambiar los datos de cadena de conexion (servidor de base de datos, usuario y contraseña) el usuario debe tener permisos de escritura

### Test Unitarios ###

* El proyecto "BusinessLogicTest" hay metodos para probar la capa de logica de negocios