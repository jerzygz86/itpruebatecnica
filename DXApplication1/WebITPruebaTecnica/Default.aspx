<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.master" CodeBehind="Default.aspx.cs" Inherits="WebITPruebaTecnica._Default" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v15.2, Version=15.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>


<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script src="Scripts/moment.min.js"></script>
     <script type="text/javascript" id="pc">

        var acciones = {
            insertar: 'insertar',
            editar: 'editar',
            ver: 'ver'
        }

        var nombresCabeceraModal = {
            insertar: 'Insertar Cliente',
            editar: 'Editar Cliente',
            ver: 'Información Cliente'
        }

        function CalcularEdad() {
            
            var fechaNacimiento = ASPxClientTextBox.Cast('pctxtFechaNacimiento').GetText();

            var pcedad = ASPxClientTextBox.Cast('pctxtEdad');
            var edad = moment().diff(moment(fechaNacimiento, "DD/MM/YYYY"), 'years');
            pcedad.SetText(edad);
            document.getElementById('<%= hfEdad.ClientID %>').value = edad
        }

                
        function MostrarVentanaModalCliente(accion) {
            
            var modalCliente = ASPxClientPopupControl.Cast("pcCliente");
            //asignar accion a realizar en el modal
            document.getElementById('<%= hfAccion.ClientID %>').value = accion;           
            
            ConfiguracionInicialModalCliente(accion);                                      

            //Asigna texto titulo del modal
            modalCliente.SetHeaderText( nombresCabeceraModal[accion]);
            pcCliente.Show();
        }

         function CargarDatosClienteSeleccionado() {

             var grid = ASPxClientGridView.Cast('gvClientes');
             var index = grid.GetFocusedRowIndex();

             grid.GetRowValues(index, "IdCliente;ID;Nombre;Apellido;Correo;Edad;FechaNacimiento", LlenarDatosCliente)
                                   
         }
         
         function LlenarDatosCliente(arrayDatosCliente) {
             
             var id = ASPxClientTextBox.Cast('pctxtID');
             var nombre = ASPxClientTextBox.Cast('pctxtNombre');
             var apellido = ASPxClientTextBox.Cast('pctxtApellido');
             var correo = ASPxClientTextBox.Cast('pctxtCorreo');
             var edad = ASPxClientTextBox.Cast('pctxtEdad');
             var fechaNacimiento = ASPxClientTextBox.Cast('pctxtFechaNacimiento');

             document.getElementById('<%= hfIdClienteSelecionado.ClientID %>').value = arrayDatosCliente[0];
             document.getElementById('<%= hfOriginalID.ClientID %>').value = arrayDatosCliente[1];

             id.SetText(arrayDatosCliente[1]);
             nombre.SetText(arrayDatosCliente[2]);
             apellido.SetText(arrayDatosCliente[3]);
             correo.SetText(arrayDatosCliente[4]);
             edad.SetText(arrayDatosCliente[5]);
             document.getElementById('<%= hfEdad.ClientID %>').value = arrayDatosCliente[5];
             fechaNacimiento.SetText(arrayDatosCliente[6]);
         }

         function LimpiarFormulario() {
             var id = ASPxClientTextBox.Cast('pctxtID');
             var nombre = ASPxClientTextBox.Cast('pctxtNombre');
             var apellido = ASPxClientTextBox.Cast('pctxtApellido');
             var correo = ASPxClientTextBox.Cast('pctxtCorreo');
             var edad = ASPxClientTextBox.Cast('pctxtEdad');
             var fechaNacimiento = ASPxClientTextBox.Cast('pctxtFechaNacimiento');
             var mensaje = ASPxClientLabel.Cast('pclblMensaje');

             id.SetText("");
             nombre.SetText("");
             apellido.SetText("");
             correo.SetText("");
             edad.SetText("");
             fechaNacimiento.SetText("");
             mensaje.SetText("");
             
             //oculta mensaje validacion 
             id.SetIsValid(true);
             nombre.SetIsValid(true);
             apellido.SetIsValid(true);
             correo.SetIsValid(true);
             edad.SetIsValid(true);
             fechaNacimiento.SetIsValid(true);
             
         }

         function ConfiguracionInicialModalCliente(accion) {
             /// <summary>Establece la configuración inicial de la ventana modal cliente dependiendo de la acción a realizar</summary>
             /// <param name="accion" type="String">Contiene el tipo de accion a realizar</param>

             LimpiarFormulario();
             var botonBorrar = ASPxClientButton.Cast('pcbtnBorrar');

             switch (accion) {
                 case acciones.ver:
                     HabilitarCamposModal(false);
                     CargarDatosClienteSeleccionado();
                     botonBorrar.SetVisible(false);

                     break;

                 case acciones.insertar:
                     HabilitarCamposModal(true);
                     botonBorrar.SetVisible(false);
                     break;

                 case acciones.editar:
                     HabilitarCamposModal(true);
                     CargarDatosClienteSeleccionado();
                     botonBorrar.SetVisible(true);
                     break;
             }

         }


         function HabilitarCamposModal(habilitarEs) {
             /// <summary>Habilita o Deshabilita campos del modal cliente</summary>
             /// <param name="habilitarEs" type="Boolean"></param>

             var id = ASPxClientTextBox.Cast('pctxtID');
             var nombre    = ASPxClientTextBox.Cast('pctxtNombre');
             var apellido = ASPxClientTextBox.Cast('pctxtApellido');
             var correo   = ASPxClientTextBox.Cast('pctxtCorreo');
             var fechaNacimiento = ASPxClientTextBox.Cast('pctxtFechaNacimiento');
             
             var botonAccion = ASPxClientButton.Cast('pcbtnAccion');

             id.SetEnabled(habilitarEs);             
             nombre.SetEnabled(habilitarEs);
             apellido.SetEnabled(habilitarEs);
             correo.SetEnabled(habilitarEs);
             fechaNacimiento.SetEnabled(habilitarEs);
             

             botonAccion.SetVisible(habilitarEs);
         }
                

        function OcultarVentanaModalCliente() {
            pcCliente.Hide();
        }         


    </script>

    <div style="width:800px; display:inline-block; margin:0px auto; padding: 0">

    <fieldset class="seccion-busqueda">
    <%--<dx:ASPxPanel ID="pnl" runat="server" Width="200px">--%>

        <div class="form-columna">
        <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text="ID" ></dx:ASPxLabel>
        <dx:ASPxTextBox runat="server" ID="txtID" Width="170px" ></dx:ASPxTextBox>
        </div>

        <div class="form-columna">
        <dx:ASPxLabel runat="server" ID="ASPxLabel2" Text="Nombre"></dx:ASPxLabel>
        <dx:ASPxTextBox runat="server" ID="txtNombre" Width="170px"></dx:ASPxTextBox>
        </div>

        <div class="form-columna">
        <dx:ASPxLabel runat="server" ID="ASPxLabel3" Text="Apellido"></dx:ASPxLabel>
        <dx:ASPxTextBox runat="server" ID="txtApellido" Width="170px"></dx:ASPxTextBox>
        </div>

        <div class="form-columna">
        <dx:ASPxLabel runat="server" ID="ASPxLabel4" Text="Correo"></dx:ASPxLabel>
        <dx:ASPxTextBox runat="server" ID="txtCorreo" Width="170px"></dx:ASPxTextBox>
        </div>

        <div class="form-columna">
        <dx:ASPxLabel runat="server" ID="ASPxLabel5" Text="Edad"></dx:ASPxLabel>
        <dx:ASPxTextBox runat="server" ID="txtEdad" Width="170px"></dx:ASPxTextBox>
        </div>
        <div class="form-columna">
        <dx:ASPxButton ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"></dx:ASPxButton>
        </div>
    </fieldset>
  
    
     <%-- DXCOMMENT: Configure ASPxGridView control --%>
    


    <dx:ASPxGridView ID="gvClientes" ClientInstanceName="gvClientes" runat="server" AutoGenerateColumns="False" OnDataBinding="gvClientes_DataBinding"
        KeyFieldName="IdCliente" Width="100%">
        <SettingsPager>
            <PageSizeItemSettings Visible="True"></PageSizeItemSettings>
        </SettingsPager>
        <Settings ShowFooter="True"></Settings>
        <SettingsBehavior AllowSelectSingleRowOnly="True" AllowSelectByRowClick="True" AllowFocusedRow="true"></SettingsBehavior>
        <Columns>
            <dx:GridViewDataColumn FieldName="IdCliente" VisibleIndex="0" Visible="false" />
            <dx:GridViewDataColumn FieldName="ID" VisibleIndex="1" />
            <dx:GridViewDataColumn FieldName="Nombre" VisibleIndex="2" />
            <dx:GridViewDataColumn FieldName="Apellido" VisibleIndex="3" />
            <dx:GridViewDataColumn FieldName="Correo" VisibleIndex="4" />
            <dx:GridViewDataColumn FieldName="Edad" VisibleIndex="5" />
            <dx:GridViewDataColumn FieldName="FechaNacimiento" VisibleIndex="6" Visible="false"/>
        </Columns>
        <Styles>
            <SelectedRow BackColor="#DFDFDF"></SelectedRow>
        </Styles>
    </dx:ASPxGridView>



    <div style="margin: 16px auto; width: 300px;">
        <div class="elemento-inline ">
         <dx:ASPxButton ID="btnVer" ClientInstanceName="btnVer"  EnableClientSideAPI="true" runat="server" Text="Ver" AutoPostBack="False" UseSubmitBehavior="false" Width="100%">
            <ClientSideEvents Click="function(s, e) { MostrarVentanaModalCliente(acciones.ver); }" />
        </dx:ASPxButton>
        </div>
        <div class="elemento-inline ">
        <dx:ASPxButton ID="btnInsertar" ClientInstanceName="btnInsertar" EnableClientSideAPI="true" runat="server" Text="Insertar" AutoPostBack="false" UseSubmitBehavior="false" Width="100%">
            <ClientSideEvents Click="function(s, e) { MostrarVentanaModalCliente(acciones.insertar); }" />
        </dx:ASPxButton>
        </div>
        <div class="elemento-inline ">
        <dx:ASPxButton ID="btnEditar" ClientInstanceName="btnEditar"  EnableClientSideAPI="true" runat="server" Text="Editar" AutoPostBack="false" UseSubmitBehavior="false" Width="100%">
            <ClientSideEvents Click="function(s, e) { MostrarVentanaModalCliente(acciones.editar); }" />
        </dx:ASPxButton>
        </div>
        <div class="elemento-inline ">
        <dx:ASPxButton ID="btnBorrar" ClientInstanceName="btnBorrar"  EnableClientSideAPI="true" runat="server" Text="Borrar" AutoPostBack="false" UseSubmitBehavior="false" Width="100%"
            OnClick="btnBorrar_Click">
            
        </dx:ASPxButton>
        </div>

    </div>

    <%-- Modal Registro / Edicion --%>
    <dx:ASPxPopupControl ID="pcCliente" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcCliente"
        HeaderText="Cliente" AllowDragging="True" PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="pcbtnAccion" Width="300px">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                         
            <div runat="server" id="contenidoModal" class="contenidoModal">
           
                    <dx:ASPxLabel runat="server" ID="ASPxLabel6" AssociatedControlID="pctxtID" Text="ID:"  CssClass="elemento-inline"/>
             
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="160px" ID="pctxtID"  CssClass="elemento-inline"
                        ClientInstanceName="pctxtID" >
                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ValidateOnLeave="true" ErrorTextPosition="Bottom">
                            <RequiredField IsRequired="True" ErrorText="ID es obligatorio" />
                        </ValidationSettings>                        
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
           
                    <dx:ASPxLabel runat="server" ID="ASPxLabel7" AssociatedControlID="pctxtNombre" Text="Nombre:" />
            
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="160px" ID="pctxtNombre"
                        ClientInstanceName="pctxtNombre" >
                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ValidateOnLeave="true" ErrorTextPosition="Bottom">
                            <RequiredField IsRequired="True" ErrorText="Nombre es obligatorio" />
                        </ValidationSettings>                        
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
               
            
                    <dx:ASPxLabel runat="server" ID="ASPxLabel8" AssociatedControlID="pctxtApellido" Text="Apellido:" />
              
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="160px" ID="pctxtApellido"
                        ClientInstanceName="pctxtApellido" >
                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ValidateOnLeave="true" ErrorTextPosition="Bottom">
                            <RequiredField IsRequired="True" ErrorText="Apellido es obligatorio" />
                        </ValidationSettings>                        
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
                
                    <dx:ASPxLabel runat="server" ID="ASPxLabel9" AssociatedControlID="pctxtCorreo" Text="Correo:" />
               
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="160px" ID="pctxtCorreo"
                        ClientInstanceName="pctxtCorreo">
                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" ValidateOnLeave="true" ErrorTextPosition="Bottom">
                            <RegularExpression ErrorText="Formato invalido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
              
                    <dx:ASPxLabel runat="server" ID="ASPxLabel10" AssociatedControlID="pctxtFechaNacimiento" Text="Fecha nacimiento:" />
               
                       
                    <dx:ASPxDateEdit runat="server" Width="160px" ID="pctxtFechaNacimiento"
                        ClientInstanceName="pctxtFechaNacimiento">
                        <ValidationSettings SetFocusOnError="True" ErrorText="" ErrorDisplayMode="Text" ValidateOnLeave="true" ErrorTextPosition="Bottom">
                            <RequiredField IsRequired="True" ErrorText="Fecha nacimiento es obligatorio" />
                        </ValidationSettings>
                        <ClientSideEvents  DateChanged="CalcularEdad" />
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxDateEdit>
                 
               
                    <dx:ASPxLabel runat="server" ID="ASPxLabel14" AssociatedControlID="pctxtEdad" Text="Edad:" />
                
                    <dx:ASPxTextBox runat="server" EnableClientSideAPI="True" Width="160px" ID="pctxtEdad" ClientEnabled="false"
                        ClientInstanceName="pctxtEdad">
                        
                        <InvalidStyle BackColor="LightPink" />
                    </dx:ASPxTextBox>
                    <asp:HiddenField ID="hfEdad"  runat="server"></asp:HiddenField>

                    <div class="contenedor-botones-modal">
             
                                 <div class="elemento-inline ">
                                        
                                    <dx:ASPxButton ID="pcbtnAccion" ClientInstanceName="pcbtnAccion" runat="server" EnableClientSideAPI="true" Text="Guardar" OnClick="pcbtnAccion_Click">
                                    </dx:ASPxButton>

                                 </div>
                                 <div class="elemento-inline ">
                                    <dx:ASPxButton ID="pcbtnSalir" ClientInstanceName="pcbtnSalir" runat="server" AutoPostBack="False" Text="Salir"
                                    CausesValidation="False">
                                    <ClientSideEvents Click="OcultarVentanaModalCliente" />
                                </dx:ASPxButton>
                                 </div>
                                 <div class="elemento-inline ">
                                    <dx:ASPxButton ID="pcbtnBorrar" ClientInstanceName="pcbtnBorrar" runat="server" AutoPostBack="False" Text="Borrar"
                                    CausesValidation="False" OnClick="pcbtnBorrar_Click">
                                </dx:ASPxButton>

                                 </div>
                                <div class="pcmSideSpacer">
                                    <dx:ASPxLabel ID="pclblMensaje" ClientInstanceName="pclblMensaje" runat="server" ClientVisible="false" ></dx:ASPxLabel> 
                                 </div>

                                <asp:HiddenField ID="hfOriginalID"  runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hfIdClienteSelecionado"  runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hfAccion"  runat="server"></asp:HiddenField>
                    </div>

                </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>

            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>


   </div>
</asp:Content>