using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic.Cliente;
using DTO.DTOs;
using DTO.Filtros;
using DevExpress.Web;

namespace WebITPruebaTecnica {
    public partial class _Default : System.Web.UI.Page {


        private const string VIEWSTATE_FILTRO_CLIENTE = "FiltroCliente";

        protected void Page_Load(object sender, EventArgs e) {

            if (!Page.IsPostBack)
            {
                gvClientes.DataBind();
                 
            }
        }

       

        #region Eventos
        protected void gvClientes_DataBinding(object sender, EventArgs e)
        {
            

            gvClientes.DataSource = BuscarClientes(ViewState[VIEWSTATE_FILTRO_CLIENTE] as FiltroCliente);
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var filtro = new FiltroCliente
                {
                    ID = txtID.Text.Trim(),
                    Nombre = txtNombre.Text.Trim(),
                    Apellido = txtApellido.Text.Trim(),
                    Correo = txtCorreo.Text.Trim(),
                    Edad = string.IsNullOrWhiteSpace(txtEdad.Text) ? 0 : int.Parse(txtEdad.Text)
                };

                ViewState[VIEWSTATE_FILTRO_CLIENTE] = filtro;                
                gvClientes.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion Eventos

        #region Metodos

        private List<ClienteDTO> BuscarClientes(FiltroCliente filtro)
        {
            try
            {
                var clienteBL = new ClienteBL();
                var clientes = clienteBL.BuscarClientes(filtro);
                
                return clientes;
            }
            catch (Exception)
            {

                throw;
            }
        }



        #endregion Metodos

        #region Eventos

        
        protected void pcbtnAccion_Click(object sender, EventArgs e)
        {
            string mensaje = string.Empty;
            try
            {
                var datosCliente = new ClienteDTO
                {
                    ID = pctxtID.Text.Trim(),
                    Nombre = pctxtNombre.Text.Trim(),
                    Apellido = pctxtApellido.Text.Trim(),
                    Correo = pctxtCorreo.Text.Trim(),
                    FechaNacimiento = pctxtFechaNacimiento.Text.Trim(),
                    Edad = string.IsNullOrWhiteSpace(hfEdad.Value) ? 0 : int.Parse(hfEdad.Value)
                };

                ClienteBL bl = new ClienteBL();

                //verifica el tipo de accion a realizar
                switch (hfAccion.Value)
                {
                    case "insertar":

                        if (bl.InsertarCliente(datosCliente, ref mensaje))
                        {
                            gvClientes.DataBind();
                            //reset datos modal
                            ASPxEdit.ClearEditorsInContainer(contenidoModal);
                            pcCliente.ShowOnPageLoad = false;
                        }
                        else
                        {
                            pcCliente.ShowOnPageLoad = true;
                            MostrarMensajeDentroModal(mensaje);
                        }
                        break;
                    case "editar":

                        datosCliente.OriginalID = hfOriginalID.Value;
                        if (bl.EditarCliente(datosCliente, ref mensaje))
                        {
                            gvClientes.DataBind();
                            ASPxEdit.ClearEditorsInContainer(contenidoModal);
                            pcCliente.ShowOnPageLoad = false;
                            //mostrar mensaje de exito
                        }
                        else
                        {
                            pcCliente.ShowOnPageLoad = true;
                            MostrarMensajeDentroModal(mensaje);

                        }

                        break;
                }

                pctxtEdad.Text = hfEdad.Value;
            }
            catch (Exception)
            {

                throw;
            }
            

        }

        protected void NameTextBox_Validation(object sender, ValidationEventArgs e)
        {
            if ((e.Value as string).Length < 2)
                e.IsValid = false;
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                //obtener Id a partir de fila seleccionada en el grid
                int idCliente =  Convert.ToInt32( gvClientes.GetRowValues(gvClientes.FocusedRowIndex, new string[] { "IdCliente"}));
                BorrarCliente(new ClienteDTO { IdCliente = idCliente});
            }
            catch (Exception)
            {

                throw;
            }          
        }

        protected void pcbtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                BorrarCliente(new ClienteDTO { IdCliente = int.Parse(hfIdClienteSelecionado.Value) });
                pcCliente.ShowOnPageLoad = true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion   

        private void BorrarCliente(ClienteDTO cliente)
        {
            try
            {
                ClienteBL bl = new ClienteBL();
                if (bl.BorrarCliente(cliente))
                {
                    gvClientes.DataBind();                                        
                }
              

            }
            catch (Exception)
            {

                throw;
            }
        }


        private void MostrarMensajeDentroModal(string mensaje)
        {
            pclblMensaje.Text = mensaje;
            pclblMensaje.Visible = true;
            pclblMensaje.ClientVisible = true;
        }
    }
}