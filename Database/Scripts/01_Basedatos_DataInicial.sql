﻿USE [master]
GO



/************ CREAR BASE DE DATOS *****************/
CREATE DATABASE [ITPruebaTecnica]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ITPruebaTecnica', FILENAME = N'C:\ITPruebaTecnica\ITPruebaTecnica.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ITPruebaTecnica_log', FILENAME = N'C:\ITPruebaTecnica\ITPruebaTecnica_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [ITPruebaTecnica] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ITPruebaTecnica].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ITPruebaTecnica] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET ARITHABORT OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [ITPruebaTecnica] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [ITPruebaTecnica] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET  DISABLE_BROKER 
GO

ALTER DATABASE [ITPruebaTecnica] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [ITPruebaTecnica] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [ITPruebaTecnica] SET  MULTI_USER 
GO

ALTER DATABASE [ITPruebaTecnica] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [ITPruebaTecnica] SET DB_CHAINING OFF 
GO

ALTER DATABASE [ITPruebaTecnica] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [ITPruebaTecnica] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [ITPruebaTecnica] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [ITPruebaTecnica] SET  READ_WRITE 
GO




/*** INSERTAR DATA INICIAL **/

USE [ITPruebaTecnica]
GO

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[FechaNacimiento] [date] NOT NULL CONSTRAINT [DF_Cliente_FechaNacimiento]  DEFAULT ('19890101'),
	[Edad] [int] NOT NULL,
	[Correo] [varchar](50) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (5, N'103', N'Megan', N'Holcomb', CAST(N'1980-04-16' AS Date), 36, N'urna.justo@dignissimmagna.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (6, N'104', N'Claudia', N'Fry', CAST(N'1989-01-01' AS Date), 27, N'arcu@elit.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (7, N'11', N'Jaime', N'Edwards', CAST(N'1989-01-01' AS Date), 27, N'Donec@luctus.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (8, N'12', N'Kasimir', N'Whitney', CAST(N'1989-01-01' AS Date), 27, N'ut.cursus.luctus@Cum.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (9, N'13', N'Dominique', N'Daniel', CAST(N'1989-01-01' AS Date), 27, N'erat@facilisisnon.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (10, N'14', N'Chadwick', N'Bryan', CAST(N'1989-01-01' AS Date), 27, N'ante.blandit@metus.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (11, N'15', N'Kylie', N'Lowery', CAST(N'1989-01-01' AS Date), 27, N'risus@estacmattis.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (12, N'16', N'Tasha', N'Dunn', CAST(N'1989-01-01' AS Date), 27, N'pharetra.ut@pretiumetrutrum.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (13, N'17', N'Quentin', N'Mcmillan', CAST(N'1989-01-01' AS Date), 27, N'et@at.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (14, N'18', N'Gil', N'Mcintosh', CAST(N'1989-01-01' AS Date), 27, N'Nam.ac@tristiqueac.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (15, N'19', N'Jenette', N'Estes', CAST(N'1989-01-01' AS Date), 27, N'dolor.vitae.dolor@etrutrumnon.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (16, N'20', N'Blythe', N'Cantu', CAST(N'1989-01-01' AS Date), 27, N'Suspendisse.eleifend@auctor.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (17, N'21', N'Xena', N'Barber', CAST(N'1989-01-01' AS Date), 27, N'netus@sapienAeneanmassa.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (18, N'22', N'Ainsley', N'Kelly', CAST(N'1989-01-01' AS Date), 27, N'magna.malesuada@aliquamadipiscing.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (19, N'23', N'Madeson', N'Whitaker', CAST(N'1989-01-01' AS Date), 27, N'gravida@duiaugue.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (20, N'24', N'Sharon', N'Tillman', CAST(N'1989-01-01' AS Date), 27, N'tellus@vehicula.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (21, N'25', N'Slade', N'Todd', CAST(N'1989-01-01' AS Date), 27, N'vulputate.dui@dictumsapienAenean.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (22, N'26', N'Octavia', N'Roach', CAST(N'1989-01-01' AS Date), 27, N'eros.Proin@mi.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (23, N'27', N'Eden', N'Day', CAST(N'1989-01-01' AS Date), 27, N'Proin.vel.arcu@ultrices.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (24, N'28', N'Noah', N'Guerra', CAST(N'1989-01-01' AS Date), 27, N'Morbi.neque.tellus@tinciduntnibh.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (25, N'29', N'Ebony', N'Sanford', CAST(N'1989-01-01' AS Date), 27, N'accumsan.convallis.ante@libero.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (26, N'30', N'Ria', N'Bradford', CAST(N'1989-01-01' AS Date), 27, N'mattis.ornare@hendreritaarcu.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (27, N'31', N'Lilah', N'Pittman', CAST(N'1989-01-01' AS Date), 27, N'Vivamus.sit.amet@Sed.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (28, N'32', N'Tanya', N'Chaney', CAST(N'1989-01-01' AS Date), 27, N'sed.hendrerit@arcuiaculis.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (29, N'33', N'Eaton', N'Gibson', CAST(N'1989-01-01' AS Date), 27, N'elit.Etiam@Fuscediam.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (30, N'34', N'Rogan', N'Pate', CAST(N'1989-01-01' AS Date), 27, N'imperdiet.ornare@anteipsum.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (31, N'35', N'Cameran', N'Frost', CAST(N'1989-01-01' AS Date), 27, N'Aenean.eget@Cras.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (32, N'36', N'Lance', N'House', CAST(N'1989-01-01' AS Date), 27, N'litora.torquent.per@facilisis.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (33, N'37', N'Nina', N'Maynard', CAST(N'1989-01-01' AS Date), 27, N'mauris@aliquamarcuAliquam.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (34, N'38', N'Vivien', N'Guerrero', CAST(N'1989-01-01' AS Date), 27, N'ac.sem.ut@quam.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (35, N'39', N'Clare', N'Booth', CAST(N'1989-01-01' AS Date), 27, N'lacus.Nulla.tincidunt@Crasvulputate.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (36, N'40', N'Pandora', N'Espinoza', CAST(N'1989-01-01' AS Date), 27, N'Nunc.sed.orci@fringillaornare.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (37, N'41', N'Cameron', N'Jones', CAST(N'1989-01-01' AS Date), 27, N'est@iaculis.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (38, N'42', N'Stephanie', N'Craig', CAST(N'1989-01-01' AS Date), 27, N'mi.Aliquam.gravida@Morbinequetellus.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (39, N'43', N'Tatum', N'Carey', CAST(N'1989-01-01' AS Date), 27, N'penatibus@tristiquesenectus.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (40, N'44', N'Armando', N'Armstrong', CAST(N'1989-01-01' AS Date), 27, N'ad@morbitristique.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (41, N'45', N'Kiona', N'Livingston', CAST(N'1989-01-01' AS Date), 27, N'tristique.neque@consectetueradipiscing.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (42, N'46', N'Hadassah', N'Vega', CAST(N'1989-01-01' AS Date), 27, N'et@arcuSedeu.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (43, N'47', N'Axel', N'Koch', CAST(N'1989-01-01' AS Date), 27, N'eu.erat@erat.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (44, N'48', N'Nina', N'Cortez', CAST(N'1989-01-01' AS Date), 27, N'urna.suscipit.nonummy@turpisvitae.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (45, N'49', N'Kasimir', N'Boone', CAST(N'1989-01-01' AS Date), 27, N'imperdiet.nec.leo@fringillaDonecfeugiat.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (46, N'5', N'Florence', N'Horne', CAST(N'1989-01-01' AS Date), 27, N'Aliquam@Nunccommodoauctor.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (47, N'50', N'Alan', N'Johns', CAST(N'1989-01-01' AS Date), 27, N'augue.ut.lacus@nuncullamcorpereu.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (48, N'51', N'Regina', N'Blevins', CAST(N'1989-01-01' AS Date), 27, N'Cum.sociis@metus.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (49, N'52', N'Samson', N'Jefferson', CAST(N'1989-01-01' AS Date), 27, N'a.magna.Lorem@semPellentesqueut.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (50, N'53', N'Carlos', N'Moreno', CAST(N'1989-01-01' AS Date), 27, N'ut.pharetra.sed@velarcu.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (51, N'54', N'Whilemina', N'Drake', CAST(N'1989-01-01' AS Date), 27, N'cubilia.Curae@auctorquistristique.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (52, N'55', N'Delilah', N'Grimes', CAST(N'1989-01-01' AS Date), 27, N'orci.Donec@ProinmiAliquam.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (53, N'56', N'Lev', N'Bender', CAST(N'1989-01-01' AS Date), 27, N'velit.eu@ultrices.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (54, N'57', N'Cheyenne', N'Riley', CAST(N'1989-01-01' AS Date), 27, N'Phasellus@nonsapienmolestie.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (55, N'58', N'Zoe', N'Ashley', CAST(N'1989-01-01' AS Date), 27, N'diam.eu@tortordictum.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (56, N'59', N'Chaim', N'Berg', CAST(N'1989-01-01' AS Date), 27, N'vehicula.aliquet@eu.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (57, N'6', N'Quinn', N'Tate', CAST(N'1989-01-01' AS Date), 27, N'lectus.sit@Aeneaneuismod.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (58, N'60', N'Kessie', N'Brady', CAST(N'1989-01-01' AS Date), 27, N'dictum@mattisvelitjusto.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (59, N'61', N'Sybil', N'Hawkins', CAST(N'1989-01-01' AS Date), 27, N'Donec@fermentumfermentum.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (60, N'62', N'Justin', N'Gibson', CAST(N'1989-01-01' AS Date), 27, N'accumsan@Aeneansedpede.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (61, N'63', N'Chester', N'Levy', CAST(N'1989-01-01' AS Date), 27, N'Duis@enim.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (62, N'64', N'Alexandra', N'Lowery', CAST(N'1989-01-01' AS Date), 27, N'molestie.tortor@sitametrisus.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (63, N'65', N'Baker', N'Holman', CAST(N'1989-01-01' AS Date), 27, N'purus@nasceturridiculusmus.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (64, N'66', N'Fatima', N'Jordan', CAST(N'1989-01-01' AS Date), 27, N'penatibus.et.magnis@nisl.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (65, N'67', N'Darrel', N'Jacobs', CAST(N'1989-01-01' AS Date), 27, N'Proin.nisl.sem@nonummyipsumnon.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (66, N'68', N'Jamal', N'Harrington', CAST(N'1989-01-01' AS Date), 27, N'Nam@Ut.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (67, N'69', N'Hanae', N'Pittman', CAST(N'1989-01-01' AS Date), 27, N'nulla@nequevitaesemper.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (68, N'7', N'Imelda', N'Mann', CAST(N'1989-01-01' AS Date), 27, N'metus.urna@eros.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (69, N'70', N'Lysandra', N'Griffith', CAST(N'1989-01-01' AS Date), 27, N'urna.et.arcu@DonectinciduntDonec.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (70, N'71', N'Yetta', N'George', CAST(N'1989-01-01' AS Date), 27, N'Sed.et.libero@semmagnanec.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (71, N'72', N'Macon', N'Mayo', CAST(N'1989-01-01' AS Date), 27, N'Curabitur.ut.odio@Nunclectus.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (72, N'73', N'Christine', N'Solomon', CAST(N'1989-01-01' AS Date), 27, N'nisi.magna.sed@metusfacilisis.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (73, N'74', N'Joseph', N'Elliott', CAST(N'1989-01-01' AS Date), 27, N'Suspendisse.aliquet.sem@famesac.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (74, N'75', N'Adrian', N'Berg', CAST(N'1989-01-01' AS Date), 27, N'sed.leo@etmagnaPraesent.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (75, N'76', N'Tanya', N'Gamble', CAST(N'1989-01-01' AS Date), 27, N'vestibulum.lorem@morbitristique.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (76, N'77', N'Rina', N'Rose', CAST(N'1989-01-01' AS Date), 27, N'vulputate.eu.odio@porttitor.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (77, N'78', N'Portia', N'Walter', CAST(N'1989-01-01' AS Date), 27, N'Integer.sem@euismodacfermentum.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (78, N'79', N'Alden', N'Pickett', CAST(N'1989-01-01' AS Date), 27, N'risus.Donec.egestas@nibh.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (79, N'8', N'Avye', N'Reyes', CAST(N'1989-01-01' AS Date), 27, N'erat.nonummy@vellectusCum.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (80, N'80', N'Jolene', N'Cline', CAST(N'1989-01-01' AS Date), 27, N'semper@malesuadafringillaest.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (81, N'81', N'Marsden', N'Taylor', CAST(N'1989-01-01' AS Date), 27, N'taciti@ac.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (82, N'82', N'April', N'Oneill', CAST(N'1989-01-01' AS Date), 27, N'magna@nonlaciniaat.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (83, N'83', N'Kenyon', N'Williamson', CAST(N'1989-01-01' AS Date), 27, N'ultricies@milorem.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (84, N'84', N'Sarah', N'Mendoza', CAST(N'1989-01-01' AS Date), 27, N'ipsum.primis@nullaInteger.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (85, N'85', N'Joan', N'Tyler', CAST(N'1989-01-01' AS Date), 27, N'at@purus.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (86, N'86', N'Abraham', N'Mckay', CAST(N'1989-01-01' AS Date), 27, N'laoreet.posuere@Nuncullamcorpervelit.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (87, N'87', N'Cleo', N'Wagner', CAST(N'1989-01-01' AS Date), 27, N'est.ac@imperdietnec.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (88, N'88', N'Ahmed', N'Daniel', CAST(N'1989-01-01' AS Date), 27, N'convallis.dolor@sagittisaugue.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (89, N'89', N'Sean', N'Mccarthy', CAST(N'1989-01-01' AS Date), 27, N'nibh.sit.amet@enim.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (90, N'9', N'Griffith', N'Solomon', CAST(N'1989-01-01' AS Date), 27, N'dis.parturient.montes@Sedmalesuada.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (91, N'90', N'Oren', N'Holmes', CAST(N'1989-01-01' AS Date), 27, N'vulputate.ullamcorper@Mauriseuturpis.ca')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (92, N'91', N'Nicholas', N'Deleon', CAST(N'1989-01-01' AS Date), 27, N'at.augue.id@nondui.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (93, N'92', N'Eaton', N'Keith', CAST(N'1989-01-01' AS Date), 27, N'sapien.Nunc@Nuncmauris.org')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (94, N'93', N'Tate', N'Mueller', CAST(N'1989-01-01' AS Date), 27, N'Ut.tincidunt.vehicula@auctornon.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (95, N'94', N'Brian', N'Travis', CAST(N'1989-01-01' AS Date), 27, N'Donec@Maecenasmalesuadafringilla.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (96, N'95', N'Kato', N'Padilla', CAST(N'1989-01-01' AS Date), 27, N'dui.Fusce@Sed.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (97, N'96', N'Kareem', N'Blackburn', CAST(N'1989-01-01' AS Date), 27, N'elit.pellentesque@convallisin.net')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (98, N'97', N'Richard', N'Riggs', CAST(N'1989-01-01' AS Date), 27, N'mi.lacinia.mattis@maurissapien.edu')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (99, N'98', N'Jillian', N'Tanner', CAST(N'1989-01-01' AS Date), 27, N'imperdiet@vestibulum.co.uk')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (100, N'99', N'Ina', N'Mccarty', CAST(N'1989-01-01' AS Date), 27, N'eget.varius@Donec.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (101, N'id1', N'Jesus', N'Rodriguez', CAST(N'1989-01-01' AS Date), 27, NULL)
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (102, N'id2', N'Cesar', N'Millán', CAST(N'1989-01-01' AS Date), 27, NULL)
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (103, N'id4', N'Andrea', N'Quintero', CAST(N'1989-01-01' AS Date), 27, N'andrea@email.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (104, N'id5', N'Jerzy', N'Guerra', CAST(N'1989-01-01' AS Date), 27, NULL)
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (105, N'id6', N'Yajaira', N'Zamora', CAST(N'1989-01-01' AS Date), 27, N'yaja@asdf.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (106, N'id7', N'Jose', N'Guerra', CAST(N'1950-10-05' AS Date), 0, N'jose@email.com')
GO
INSERT [dbo].[Cliente] ([IdCliente], [ID], [Nombre], [Apellido], [FechaNacimiento], [Edad], [Correo]) VALUES (107, N'150', N'alejandro', N'Silva', CAST(N'2010-04-22' AS Date), 6, N'Nunc.ullamcorper@Nulla.com')
GO
SET IDENTITY_INSERT [dbo].[Cliente] OFF
GO
